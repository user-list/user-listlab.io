export const config = {
  idFieldName: 'userId',
  noAnimationSettings: {
    showClass: {
      popup: ''
    },
    hideClass: {
      popup: ''
    },
  },
};

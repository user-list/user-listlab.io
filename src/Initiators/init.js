import $ from 'jquery';
import 'datatables.net-dt';
import { normalizeName } from './../Utils/normalizeName';
import { config } from './../Config/config';
import { showUserInfo } from './../Services/showUserInfo';

export const init = () => {
  $.getJSON('https://hr.oat.taocloud.org/v1/users?limit=2000', arrayOfShortUsers => {

    const $table = $('<table/>', {
      'class': 'short-users-table'
    });

    let cellTitles = '';
    for (let key in arrayOfShortUsers[0]) {
      cellTitles += `<th>${key}</th>`;
    }
    const $tableHead = $(`<thead><tr>${cellTitles}</tr></thead>`);
    $tableHead.appendTo($table);

    const items = [];
    arrayOfShortUsers.forEach(user => {
      let row = '';
      for (let key in user) {
        const value = user[key];
        row += `<td>${normalizeName(value)}</td>`;
      }
      items.push(`<tr data-id=${user[config.idFieldName]} class="user-row js-user-row">${row}</tr>`);
    });
   
    $('<tbody/>', {
      html: items.join('')
    }).appendTo($table);

    $table.appendTo('#root');

    $table.on('click', '.js-user-row', e => {
      const userId = $(e.target).closest('.js-user-row').attr('data-id');
      showUserInfo(userId);
    });

    $table.dataTable();
  });
}

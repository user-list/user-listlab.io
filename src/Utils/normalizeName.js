import { startCase } from 'lodash';

export const normalizeName = str => {
  return startCase(str);
}

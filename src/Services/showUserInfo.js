import Swal from 'sweetalert2'
import { config } from './../Config/config';

export const showUserInfo = userId => {
  const endpoint = `https://hr.oat.taocloud.org/v1/user/${userId}`;

  Swal.queue([{
    title: `User ID: ${userId}`,
    confirmButtonText: `Get User Info`,
    text:
      `User info will be received
      via AJAX request`,
    showLoaderOnConfirm: true,
    ...config.noAnimationSettings,
    preConfirm: () => {
      return fetch(endpoint)
        .then(response => response.json())
        .then(user => {
          let htmlString = '';
          for (let key in user) {
            htmlString += `<p><b>${key}</b>: ${user[key]}</p>`;
          }
          const presentationObj = {
            html: htmlString,
            imageUrl: user.picture,
            imageWidth: 128,
            imageHeight: 128,
            imageAlt: 'User photo',
            ...config.noAnimationSettings,
          };
          Swal.insertQueueStep(presentationObj);
        })
        .catch(() => {
          Swal.insertQueueStep({
            icon: 'error',
            title: 'Unable to get user info'
          })
        })
    }
  }]);
};
